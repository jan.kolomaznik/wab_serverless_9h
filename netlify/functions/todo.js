import * as  mongoose from "mongoose";


const MONGO_URI = process.env['MONGO_URI'];
if (!MONGO_URI) {
    throw new Error(`Missing enviroment variable MONGO_URI`)
}

const todoSchema = mongoose.Schema({
    created: { type: Date, default: Date.now },
    text: { type: String, required: true, trim: true },
    done: { type: Boolean, default: false }
})
const Todo = mongoose.model("Todo", todoSchema)


async function storeTodo(data) {
    let client = null;
    try {
        client = await mongoose.connect(MONGO_URI);
        const newTodo = new Todo(data);
        const savedTodo = await newTodo.save()
        return {
            statusCode: 201,
            body: JSON.stringify(savedTodo)
        }  
    } finally {
        if (client) {
            await client.disconnect()
            client = null;
        }
    }
    
  
}

export const handler = async (event) => {
    switch (event.httpMethod) {
        case "POST":
            return storeTodo(JSON.parse(event.body));
        default:
            return {
                statusCode: 405,
                body: "Method Not Allowed"
            }
    }
}